
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author rafaelcarlos
 */
public class Temp {
    public static void main(String[] args) {
          int op;
        float raio = 0.0f;
        float area = 0.0f;
        float per = 0;
        float pi = 3.14f;

        op = Integer.parseInt(JOptionPane.showInputDialog("Informe com a operacao"));

        raio = Integer.parseInt(JOptionPane.showInputDialog("Informe o raio"));

        if (op == 1) {
            area = pi * (raio * raio);

            JOptionPane.showMessageDialog(null, "A area e: " + area);
        } else if (op == 2) {
            per = (2 * pi * raio);
            JOptionPane.showMessageDialog(null, " O perimetro e: " + per);
        } else if (op != 1 && op != 2) {
            JOptionPane.showMessageDialog(null, "O indicador de operação foi mal fornecido!");
        }

    }
}
