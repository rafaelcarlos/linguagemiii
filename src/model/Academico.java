package model;

/**
 *
 * @author fredsonvieiracosta
 */
public class Academico {

    private String matricula;
    private String nome;
    private String curso;
    private int periodo;

    public Academico() {
    }

    public Academico(String matricula, String nome, String curso, int periodo) {
        this.matricula = matricula;
        this.nome = nome;
        this.curso = curso;
        this.periodo = periodo;
    }

    /**
     * @return the matricula
     */
    public String getMatricula() {
        return matricula;
    }

    /**
     * @param matricula the matricula to set
     */
    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the curso
     */
    public String getCurso() {
        return curso;
    }

    /**
     * @param curso the curso to set
     */
    public void setCurso(String curso) {
        this.curso = curso;
    }

    /**
     * @return the periodo
     */
    public int getPeriodo() {
        return periodo;
    }

    /**
     * @param periodo the periodo to set
     */
    public void setPeriodo(int periodo) {
        this.periodo = periodo;
    }

    @Override
    public String toString() {
        return "Academico{" + "matricula=" + matricula + ", nome=" + nome + ", curso=" + curso + ", periodo=" + periodo + '}';
    }

}
