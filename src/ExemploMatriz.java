

import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 *
 * @author fredsonvieiracosta
 */
public class ExemploMatriz extends JFrame{
    private String[] colunas = {"Matrícula", "Nome", "Curso", "Período"};
    private Object[][] dados = {
        {"123", "Fredson", "Sistemas de Informação", "3"},
        {"234", "Vieira", "Ciência da Computação", "5"},
        {"345", "Costa", "Engenharia da Computação", "7"}
    };

    public ExemploMatriz() {
        this.setLayout(new GridLayout(1, 1));
        JScrollPane painel = new 
            JScrollPane(new JTable(this.dados, this.colunas));
        this.add(painel);
        
        this.pack();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }
    
    
    public static void main(String[] args) {
        new ExemploMatriz();
    }
}
