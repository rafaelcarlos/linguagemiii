/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package relogio;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.HeadlessException;
import javax.swing.JFrame;

/**
 *
 * @author fredsonvieiracosta
 */
public class Principal extends JFrame{
    
    private int angulo;

    public Principal() throws HeadlessException {
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setSize(1024, 768);        
        this.setVisible(true);
        this.angulo = 0;
    }
    
    @Override
    public void paint(Graphics g)
    {
        
        g.setColor(Color.black);
        g.fillRect(100, 100, 110, 10);
//        g.fillRect(100, 100, 10, 100);
        g.fillRect(200, 100, 10, 100);
        g.fillRect(100, 200, 10, 100);
//        g.fillRect(200, 200, 10, 100);
        g.fillRect(100, 200, 110, 10);
        g.fillRect(100, 300, 110, 10);
//        g.setColor(Color.red);
//        //texto, x, y
//        g.drawString("Teste de String", 200, 200);
//        
//        g.setColor(Color.blue);
//        //x, y, largura, altura
//        g.fillRect(400, 400, 200, 200);
//        
//        g.setColor(Color.green);
//        //x, y, largura, altura, angulo inicial, angulo final
//        g.fillArc(600, 100, 200, 200, 0, this.angulo);
//        this.angulo++;
//        
//        g.setColor(Color.yellow);
//        g.fillRoundRect(400, 100, 100, 100, 10, 10);
          g.setColor(Color.black);
          g.fillArc(300, 100, 400, 400, 360-(38*6)+90, 38*6);
          g.setColor(Color.blue);
          g.fillArc(350, 150, 300, 300, 360-(55*6)+90, 55*6);
          g.setColor(Color.red);
          g.fillArc(400, 200, 200, 200, 360-(7*30)+90, 7*30);
    }
    
    public static void main(String[] args) {
        Principal p = new Principal();
//        while (p.angulo < 360)
//        {
//            p.repaint();
//        }
        
    }
    
}
