
import javax.swing.JOptionPane;

/**
 *
 * @author rafaelcarlos
 */
public class Calculadora {
    
    public static void main(String[] args) {
        for (int opcao = 0; opcao >= 0;) {
            opcao = Integer.parseInt(JOptionPane.showInputDialog(null,
                    "Soma - 1,"
                    + " Multiplicação - 2,"
                    + " Divisão - 3,"
                    + " Subtração - 4 e"
                    + " Sair - 5", "Escolha uma opção"));
            if (opcao == 1) {
                Soma();
            } else if (opcao == 2) {
                Multiplicacao();
            } else if (opcao == 3) {
                Divisao();
            } else if (opcao == 4) {
                Subtracao();
            } else if (opcao == 5) {
                System.exit(0);
            } else {
                JOptionPane.showMessageDialog(null, "Opção Invalida", "ERRO", JOptionPane.ERROR_MESSAGE);
            }
        }
        
    }
    
    public static void Soma() {
        float val1f = 0, val2f = 0, resultf = 0;
        val1f = Integer.parseInt(JOptionPane.showInputDialog("Digite o valor 1"));
        val2f = Integer.parseInt(JOptionPane.showInputDialog("Digite o valor 2"));
        
        resultf = val1f + val2f;
        JOptionPane.showMessageDialog(null, "O resultado e: " + resultf);
    }
    
    public static void Multiplicacao() {
        float val1f = 0, val2f = 0, resultf = 0;
        val1f = Integer.parseInt(JOptionPane.showInputDialog("Digite o valor 1"));
        val2f = Integer.parseInt(JOptionPane.showInputDialog("Digite o valor 2"));
        
        resultf = val1f * val2f;
        JOptionPane.showMessageDialog(null, "O resultado e: " + resultf);
    }
    
    public static void Divisao() {
        float val1f = 0, val2f = 0, resultf = 0;
        val1f = Integer.parseInt(JOptionPane.showInputDialog("Digite o valor 1"));
        val2f = Integer.parseInt(JOptionPane.showInputDialog("Digite o valor 2"));
        
        resultf = val1f / val2f;
        JOptionPane.showMessageDialog(null, "O resultado e: " + resultf);
    }
    
    public static void Subtracao() {
        float val1f = 0, val2f = 0, resultf = 0;
        val1f = Integer.parseInt(JOptionPane.showInputDialog("Digite o valor 1"));
        val2f = Integer.parseInt(JOptionPane.showInputDialog("Digite o valor 2"));
        
        resultf = val1f - val2f;
        JOptionPane.showMessageDialog(null, "O resultado e: " + resultf);
        
    }
}
